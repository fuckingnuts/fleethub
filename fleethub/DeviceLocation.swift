//
//  DeviceLocation.swift
//  fleethub
//
//  Created by Ammar Naqvi on 2/28/17.
//  Copyright © 2017 Ammar Naqvi. All rights reserved.
//

import JSONJoy

public struct DevicesLocation : JSONJoy {
    
    var devices : [DeviceLocation]
    
    public init(_ decoder: JSONDecoder) throws {
        devices = try decoder["devices"].get()
    }
}

public struct DeviceLocation : JSONJoy {
    
    var deviceId : String
    var longitude : String
    var latitude : String
    var speed : String
    var ignition : String
    var temperature : String
    var compass : String
    var createDate : String!
    var updateDate : String!
    
    
    public init(_ decoder: JSONDecoder) throws {
        deviceId = try decoder["deviceId"].get()
        longitude = try decoder["longitude"].get()
        latitude = try decoder["latitude"].get()
        speed = try decoder["speed"].get()
        ignition = try decoder["ignition"].get()
        temperature = try decoder["temperature"].get()
        compass = try decoder["compass"].get()
        createDate = decoder["createDate"].getOptional()
        updateDate = decoder["updateDate"].getOptional()
    }
}
