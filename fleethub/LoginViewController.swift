//
//  ViewController.swift
//  fleethub
//
//  Created by Ammar Naqvi on 3/16/17.
//  Copyright © 2017 Ammar Naqvi. All rights reserved.
//

import UIKit
import SwiftHTTP
import JSONJoy
import Material

class LoginViewController: UIViewController {
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate var emailField: ErrorTextField!
    fileprivate var passwordField: TextField!
    
    /// A constant to layout the textFields.
    fileprivate let constant: CGFloat = 32
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.grey.lighten5
        
        preparePasswordField()
        prepareEmailField()
        prepareLoginResponderButton()
    }
    
    /// Prepares the resign responder button.
    fileprivate func prepareLoginResponderButton() {
        let btn = RaisedButton(title: "Login", titleColor: Color.blue.base)
        btn.addTarget(self, action: #selector(handleLoginResponderButton(button:)), for: .touchUpInside)
        
        view.layout(btn).width(150).height(constant).center(offsetY: -passwordField.height + 160)
    }
    
    /// Handle the resign responder button.
    @objc
    internal func handleLoginResponderButton(button: UIButton) {
        emailField?.resignFirstResponder()
        passwordField?.resignFirstResponder()
        let password: String! = passwordField.text;
        let email: String! = emailField.text;
       
        let params = ["email": email, "password": password]
        do {
            let opt = try HTTP.POST("http://111.68.101.29/ivms/rest/ws/customerLogin", parameters: params)
            opt.start { response in
                if let error = response.error {
                    print("got an error: \(error)")
                    return
                }
                do {
                    let user = try User(JSONDecoder(response.text ?? ""))
                    Util.sharedInstance.setUser(user: user)
                    for device in user.userAssignedDevices {
                        print(device.owner)
                    }
                    //That's it! The object has all the appropriate properties mapped.
                    
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "showDevices", sender: self)
                        
                    }
                } catch let error {
                    print("Error in parsing json: \(error)")
                }
            }
        } catch let error {
            print("got an error creating the request: \(error)")
        }
    }

}


extension LoginViewController {
    fileprivate func prepareEmailField() {
        emailField = ErrorTextField()
        emailField.placeholder = "Email"
        emailField.detail = "Error, incorrect email"
        emailField.isClearIconButtonEnabled = true
        emailField.delegate = self
        
        // Set the colors for the emailField, different from the defaults.
        //        emailField.placeholderNormalColor = Color.amber.darken4
        //        emailField.placeholderActiveColor = Color.pink.base
        //        emailField.dividerNormalColor = Color.cyan.base
        //        emailField.dividerActiveColor = Color.green.base
        let leftView = UIImageView()
        leftView.image = #imageLiteral(resourceName: "ic_perm_identity")
        emailField.leftView = leftView
        emailField.leftViewMode = UITextFieldViewMode.always
        view.layout(emailField).center(offsetY: -passwordField.height - 60).left(20).right(20)
    }
    
    fileprivate func preparePasswordField() {
        passwordField = TextField()
        passwordField.placeholder = "Password"
        passwordField.detail = "At least 8 characters"
        passwordField.clearButtonMode = .whileEditing
        passwordField.isVisibilityIconButtonEnabled = true
        
        let leftView = UIImageView()
        leftView.image = #imageLiteral(resourceName: "ic_lock_outline")
        passwordField.leftView = leftView
        passwordField.leftViewMode = UITextFieldViewMode.always
        
        // Setting the visibilityIconButton color.
        passwordField.visibilityIconButton?.tintColor = Color.green.base.withAlphaComponent(passwordField.isSecureTextEntry ? 0.38 : 0.54)
        
        view.layout(passwordField).center().left(20).right(20)
    }
}


extension UIViewController: TextFieldDelegate {
    public func textFieldDidEndEditing(_ textField: UITextField) {
        (textField as? ErrorTextField)?.isErrorRevealed = true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }
}

