    //
    //  Device.swift
    //  fleethub
    //
    //  Created by Ammar Naqvi on 2/26/17.
    //  Copyright © 2017 Ammar Naqvi. All rights reserved.
    //
    
    import JSONJoy
    
    
    public struct Device : JSONJoy {
        var id : Int
        var owner : String
        var deviceId : String
        var description : String
        var statusCode : Int
        var appId : Int
        var appnName : String
        var createDate : String!
        var updateDate : String!
        
        public init(_ decoder: JSONDecoder) throws {
            id = try decoder["id"].get()
            owner = try decoder["owner"].get()
            deviceId = try decoder["deviceId"].get()
            description = try decoder["description"].get()
            statusCode = try decoder["statusCode"].get()
            appId = try decoder["appId"].get()
            appnName = try decoder["appnName"].get()
            createDate = decoder["createDate"].getOptional()
            updateDate = decoder["updateDate"].getOptional()
        }
    }
	
