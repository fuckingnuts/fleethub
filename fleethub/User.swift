//
//  User.swift
//  fleethub
//
//  Created by Ammar Naqvi on 2/26/17.
//  Copyright © 2017 Ammar Naqvi. All rights reserved.
//

import JSONJoy

public struct User : JSONJoy {
    var userType : Int
    var clientId : String
    var company : String
    var email : String
    var username : String
    var statusCode : Int
    var createDate : String
    var updateDate : String
    var commServerIndicator : Int
    var userAssignedDevices : [Device]
    var zoneAssignmentLimit : Int
    var ivmsDashboard : Int
    var ivmsDriverPerformance : Int
    var ivmsRoutes : Int
    var ivmsUserActivity : Int
    
    public init(_ decoder: JSONDecoder) throws {
        userType = try decoder["userType"].get()
        clientId = try decoder["clientId"].get()
        company = try decoder["company"].get()
        email = try decoder["email"].get()
        username = try decoder["username"].get()
        statusCode = try decoder["statusCode"].get()
        createDate = try decoder["createDate"].get()
        updateDate = try decoder["updateDate"].get()
        commServerIndicator = try decoder["commServerIndicator"].get()
        userAssignedDevices	 = try decoder["userAssignedDevices"].get()
        zoneAssignmentLimit = try decoder["zoneAssignmentLimit"].get()
        ivmsDashboard = try decoder["ivmsDashboard"].get()
        ivmsDriverPerformance = try decoder["ivmsDriverPerformance"].get()
        ivmsRoutes = try decoder["ivmsRoutes"].get()
        ivmsUserActivity = try decoder["ivmsUserActivity"].get()
    }
}
